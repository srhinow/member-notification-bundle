<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension member-notification-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

/*
 * Legends
 */
$GLOBALS['TL_LANG']['tl_member']['notification_legend'] = 'Benachrichtigungs-Einstellungen';

/*
 * Fields
 */
$GLOBALS['TL_LANG']['tl_member']['notificationId'] = [
    'Benachrichtigung',
    'Wählen Sie eine Benachrichtigung aus die beim speichern gesendet an dieses Mitglied gesendet wird.',
];

/*
 * Messages
 */
$GLOBALS['TL_LANG']['tl_member']['notification_send_error']
    = 'Beim Versand der Notifikation ist ein Fehler aufgetreten.';
$GLOBALS['TL_LANG']['tl_member']['notification_send_success']
    = 'Die Notification wurde erfolgreich gesendet.';
